Installation de Kali Linux sous Windows 10 avec un accès graphique sans installation d'un logiciel supplémentaire, sous le bureau XFCE.

Vous devez déjà activer le système WSL. Pour cela lancer un PowerShell en mode administrateur et taper la commande suivante :

""Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Windows-Subsystem-Linux""

Vous téléchargerez ensuite Kali Linux sur le Windows store. Puis le lancer pour finaliser l'installation, renseigner le nom d'utilisateur et le mot de passe de session.

Mettre à jour la distribution :

""sudo apt update"


""sudo apt dist-upgrade""

Si vous souhaitez installer des paquets supplémentaires regroupant des logiciels dédiés à un thème particulier, lister les paquets disponibles :

""sudo apt list kali-linux-*""

/!\/!\/!\/!\/!\ Lors de l'utilisation des logiciels vous serez loggé en tant qu'utilisateurs !!!! Au contraire d'un kali normale ou vous êtes en root. Donc soit sudo -s , ou créer une session root /!\/!\/!\/!\/!\

Puis installer les paquets suivant :

"" apt install kali-desktop-xfce xorg xrdp""

Puis lancer le serveur XRDP avec la commande:

""sudo /etc/init.d/xrdp start""

/!\/!\/!\ Relever le numéro de port afficher /!\/!\/!\

Laisser la fenêtre de Kali ouverte, et lancer l'application de contrôle à distance. Dans la barre de recherche de Windows 10 vous tapez MST.


Vous renseignez pour "Ordinateur"  127.0.0.1:3389

La connexion se lance alors ;)